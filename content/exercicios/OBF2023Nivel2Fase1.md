---
title: "OBF 2023"
date: 2023-010-02T16:08:38+02:00
description: "Nível 2 | Fase 1"
images:
  - /olimpicohub/images/OBF2023.png
---
## Nível 2 | Fase 1
Você pode acessar a prova [aqui](https://app.graxaim.org/obf/2023/ga/fase1_n2.pdf).
### Questão 1
Ao ser abandonada, a cápsula será acelerada pela gravidade lunar. Pela equação de Torricelli:

$$
v^2 = v_0^2 + 2gh
$$

logo:

$$
v = \sqrt{v_0^2 + 2gh}
$$

Substituindo os valores numéricos:

$$
v = \sqrt{16.8} \approx 4.1  \text{m/s}
$$

Portanto **Item b)**.
