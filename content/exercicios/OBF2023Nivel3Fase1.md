---
title: "OBF 2023"
date: 2023-010-02T16:08:38+02:00
description: "Nível 3 | Fase 1"
images:
  - /olimpicohub/images/OBF2023.png
---
## Nível 3 | Fase 1
Você pode acessar a prova [aqui](https://app.graxaim.org/obf/2023/ga/fase1_n3.pdf).
### Questão 1
A intensidade máxima resultante da interferência de duas ondas é encontrada quando as amplitudes se somam. A intensidades é proporcional ao quadrado da amplitude, então temos que:

$$
I_1 = kA_1^2 = 4I_2
$$

$$
I_2 = kA_2^2
$$

A intensidade resultante então será:

$$
I_M = k(A_1 + A_2)^2
$$

$$
I_M = kA_1^2 + 2kA_2^2 + kA_1A_2
$$

Note que $$kA_1A_2 = \sqrt{I_1I_2}$$, logo temos:

$$
I_M = 9I_2
$$

Fazendo o mesmo processo para o caso mínimo:

$$
I_M = k(A_1 - A_2)^2
$$

$$
I_M = I_2
$$

Logo a razão é 9/1. Portanto **Item d\)**.

A intensidade resultante da interferência de duas ondas pode ser calculada de uma maneira mais geral usando a expressão:

$$
I = I_1 + I_2 + 2\sqrt{I_1I_2}\cos(\phi)
$$

Em que \\(I_1\\) e \\(I_2\\) são as intensidades das ondas originais e \\(\phi\\) é a diferença de fase entre as ondas.
