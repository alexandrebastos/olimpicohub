---
title: "OBF 2023"
date: 2023-010-02T16:08:38+02:00
description: "Nível 1 | Fase 1"
images:
  - /olimpicohub/images/OBF2023.png
---
## Nível 1 | Fase 1
Você pode acessar a prova [aqui](https://app.graxaim.org/obf/2023/ga/fase1_n1.pdf).
### Questão 1
A energia potencial de um corpo de massa \( m \) sob um campo gravitacional uniforme, \( g \), depende da altura em relação ao solo da seguinte forma:

$$
U = mgh
$$

Desse modo, para encontrarmos a energia em função do tempo, precisamos encontrar a altura do corpo em função do tempo. Nesse sentido, o corpo está em queda livre, ou seja, está sob ação apenas da gravidade.

Assim pela segunda lei de Newton,

$$
F = ma
$$

$$
mg = ma
$$

$$
a = g
$$

O corpo está sob aceleração constante. Com isso, pelas equações da cinemática:

$$
h = h_0 - \frac{{gt^2}}{2}
$$

Perceba que o sinal negativo vem do fato da aceleração ser contrária ao sentido positivo da altura. Portanto, a energia potencial em função do tempo é

$$
U = mgh_0 - \frac{{mg t^2}}{2}
$$

Essa descreve uma parábola em função do tempo com concavidade para baixo, logo o item que melhor representa o comportamento é o **Item c)**.
