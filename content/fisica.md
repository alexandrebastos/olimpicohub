+++
title = 'Física'
date = 2023-10-03T02:20:56-03:00
draft = false
+++

A física representa a ciência natural mais essencial e abrangente, abordando uma vasta gama de fenômenos, desde as interações no núcleo atômico até os movimentos das galáxias. Ao fornecer o formalismo necessário para compreender as complexas interações da natureza, imergir-se na física oferece a oportunidade de desenvolver sua criatividade e intuição, capacitando-o a modelar fenômenos físicos de forma independente.

### Olimpíada Brasileira de Física

A **OBF** (Olimpíada Brasileira de Física) é dividida em três fases e possui três níveis:

- A *primeira fase* consiste em 20 questões de múltipla escolha abrangendo uma variedade de tópicos do programa relacionados ao nível da prova.

- A *segunda fase* é composta por duas partes: uma em que apenas as respostas devem ser fornecidas e outra em que é necessário apresentar a solução teórica completa.

- Na *terceira fase*, os candidatos podem escolher responder a 8 questões de sua escolha ou enfrentar 8 questões obrigatórias, dependendo de sua série. Todas as questões são abertas e geralmente abordam mais de um tópico.

Os três níveis são definidos da seguinte forma:

- **Nível 1**: Destinado a alunos do 8º e 9º anos do Ensino Fundamental.

- **Nível 2**: Destinado a alunos do 1º e 2º anos do Ensino Médio.

- **Nível 3**: Destinado a alunos do 3º ano do Ensino Médio e do 4º ano do Ensino Técnico.

No *primeiro ano*, os alunos mais bem classificados (todos os medalhistas de ouro e alguns de prata) são selecionados para o processo de seleção das *OIF's* (Olimpíadas Internacionais de Física).

No *segundo ano*, a pontuação obtida na OBF será levada em consideração para os alunos classificados na seletiva, e isso definirá quem terá a oportunidade de participar da repescagem. A repescagem é uma chance para os alunos que não foram selecionados na seletiva no primeiro ano, mas se destacaram no segundo ano, participarem do processo seletivo.


